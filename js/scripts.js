$(function() {
	$('.fancybox').click(function(e) {
		  e.preventDefault();

      $.fancybox(
        '<video id="video" width="' + $(this).attr('data-width') +
  			'" height="' + $(this).attr('data-height') +
  			'" preload="auto" controls="controls" autoplay="autoplay" class="video-js vjs-default-skin"><source src="'
  			+ $(this).attr('href') + '" type="video/mp4" /><div>Your browser does not support the HTML5 video tag.</div></video>'
        ,
        {
        'padding': 0,
  			'autoScale': false,
  			'transitionIn': 'none',
  			'transitionOut': 'none',
  			'title': $(this).attr('data-title'),
  			'width': $(this).attr('data-width'),
  			'height': $(this).attr('data-height'),
        'autoDimensions': false,
        'enableEscapeButton': true,
        'onComplete': function() {
  				// do this for the dynamically loaded video
  				videojs('video', {}, function(){});
          $.fancybox.resize();
  			},
        'onCleanup': function() {
  				// if you do not do this the video will keep downloading in the background
  				videojs('video').dispose();
  			}
  		});

	   return false;
	});

});
